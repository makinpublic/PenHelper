package tut.makinen.attacks;

import tut.makinen.TinyHTML.Form;
import tut.makinen.TinyHTML.Input;
import tut.makinen.WebSnooper.ConnectionHandler;
import tut.makinen.WebSnooper.FileHandler;
import tut.makinen.WebSnooper.HttpParser;

import java.util.Vector;

public class SqlInjectionProbe {
    private ConnectionHandler connectionHandler;
    private FileHandler fileHandler;
    private String subUrl;
    private String cookie;

    String[] probes = {"yes'no",
                        "yes\'no",
                        "yes\\\'no",
                        "yes\"no",
                        "yes\\\"no",
                        "yes--no",
                        "yes/*no",
                        "yes##no",
                        "yes;no",
                        "yes:no",
                        "yes@@no",
                        "'yes'+'no'",
                        "'yes''no'",
                        "'yes'||'no'",
                        "''yes''no''"};

    public SqlInjectionProbe(ConnectionHandler connectionHandler, String subUrl, String cookie, String folder){
        this.connectionHandler = connectionHandler;
        this.fileHandler = new FileHandler(folder, 1);
        this.subUrl = subUrl;
        this.cookie = cookie;
    }

    public void setTarget(String subUrl, String cookie){
        this.subUrl = subUrl;
        this.cookie = cookie;
    }

    public void run(){
        String http = connectionHandler.httpGET("", subUrl, cookie);
        Vector<Form> forms =  HttpParser.parseInputs(http);

        for(String probe : probes){
            for(Form form : forms){
                for(Input input : form.getInputs()){
                    input.setValue(probe);
                }
                for(Input submit : form.getSubmit()){
                    String input =  form.toUrl(submit);
                    String result = connectionHandler.httpGET(subUrl + "/" + input);
                    fileHandler.addResult("sqlProbe", input, connectionHandler.stripHttpHeader(result));
                }
                String input =  form.toUrl(null);
                String result = connectionHandler.httpGET(subUrl + "/" + input);
                fileHandler.addResult("sqlProbe", input, connectionHandler.stripHttpHeader(result));
            }
        }
    }
}
