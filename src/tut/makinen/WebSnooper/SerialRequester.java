package tut.makinen.WebSnooper;
import tut.makinen.TinyHTML.Form;
import tut.makinen.TinyHTML.Input;
import tut.makinen.attacks.WhiteNoiseFuzz;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Vector;

public class SerialRequester {
    Vector<Form> forms;
    Integer id = 0;
    ConnectionHandler connectionHandler;
    private String subUrl;
    private FileHandler fileHandler;
    boolean decoder = true;

    public SerialRequester(Vector<Form> forms, ConnectionHandler connectionHandler, String subUrl, String folder){
        this.forms = forms;
        this.connectionHandler = connectionHandler;
        this.subUrl = subUrl;
        fileHandler = new FileHandler(folder, 1);
    }

    private String vectorToString(Vector<Input> vector){
        String ret = "";
        for(Input input : vector){
            ret += (ret.length() > 0 ? "." : "") + input.getName();
        }
        return ret;
    }

    public void makeRequest(int times){
        for(int i = 0; i < times; i++){
            for(Form form : forms){
                for(Vector<Input> inp : form.getAllInputCombinations()){
                    for(Input submit : form.getSubmit()){
                        for(int ii = 0; ii < 3; ii++) {
                            try {
                                String input = WhiteNoiseFuzz.inputToNoiseUrl(inp, submit, ii);
                                String decodedInput = (decoder ? URLDecoder.decode(input, "UTF-8") : input);
                                String result = connectionHandler.httpGET(subUrl+"/"+input);
                                fileHandler.addResult("WhiteNoiseFuzz" + "_" +  vectorToString(inp) + "_" + submit.getName() + "_", decodedInput, connectionHandler.stripHttpHeader(result));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    for(int ii = 0; ii < 3; ii++) {
                        try {
                            String input = WhiteNoiseFuzz.inputToNoiseUrl(inp, null, ii);
                            String decodedInput = (decoder ? URLDecoder.decode(input, "UTF-8") : input);
                            String result = connectionHandler.httpGET(subUrl+"/"+input);
                            fileHandler.addResult("WhiteNoiseFuzz" + "_" +  vectorToString(inp) + "_nullSubmit_", decodedInput, connectionHandler.stripHttpHeader(result));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
