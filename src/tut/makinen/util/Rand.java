package tut.makinen.util;

import java.security.SecureRandom;

public class Rand extends SecureRandom {
    String acceptableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz1234567890";
    String extendedSymbols = ".,_!#()[]{}";
    String showStoppers = "=?;:$&/\\\"\'#+-";

    /**
     *
     * @param length
     * @param level of how bad the characters are.
     *              0 for A-z, 0-9;
     *              1 to add some symbols,
     *              2 for symbols often found in programming languages, and
     *              3 for all charvalues from 0 to 255
     * @return
     */
    public String nextString(int length, int level){
        String s = "";

        //Expect the unexpected
        if(level == 3){
            for(int i = 0; i < length; i++){
                s += (char) nextInt(255);
            }
            return s;
        }

        //Expect the expected
        String characters = acceptableCharacters + (level > 0 ? extendedSymbols : "") + (level > 1 ? showStoppers : "");
        int max = characters.length() - 1;
        for(int i = 0; i < length; i++){
            s += characters.charAt(nextInt(max));
        }
        return s;
    }

    public String nextString(int length){
        return nextString(length, 0);
    }
}
