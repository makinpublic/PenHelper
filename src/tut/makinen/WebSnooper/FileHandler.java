package tut.makinen.WebSnooper;

import java.io.*;
import java.util.Vector;
import java.util.regex.Pattern;

public class FileHandler {
    private String folder;
    private int minDiff;
    private Vector<String> existingResults;
    private int uid;
    private final static String CONTENT_HEADER = "<h1>|=====RESULT=====|</h1>";
    private final static String HTML_PRE = "<!DOCTYPE html><html><body><h1>|=====INPUT=====|</h1>";
    private final static String HTML_POST = "</body></html>";

    public FileHandler(String folder, int minDiff){
        this.folder = folder;
        this.minDiff = minDiff;
        existingResults = new Vector<>();
        uid = 0;
    }

    private String read(String fileName){
        String result = "";
        try {
            File file = new File(folder + "/" + fileName);
            if(!file.exists()) return "";
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) != null){
                result += line + '\n';
            }
            br.close();
        } catch(Exception e) {
            e.printStackTrace();
            return "";
        }
        return result;
    }

    public void write(String fileName, String contents){
        try {
            File file = new File(folder + "/" + fileName);
            if(!file.exists()){
                file.createNewFile();
            } else {
                //throw new Exception("File already exists!");
                //Let's just overwrite
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(contents);
            bw.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean filesDiffer(String fileA, String fileB){
        int i = 0;
        int diff = 0;
        while(i < fileA.length() && i < fileB.length()){
            if(fileA.charAt(i) != fileB.charAt(i)){
                diff++;
                if(diff >= minDiff){
                    return true;
                }
            }
            i++;
        }
        return false;
    }

    private boolean newFileIsUnique(String newFile){
        boolean similarityFound = false;
        for(String existingFile : existingResults){
            if(!filesDiffer(existingFile, newFile)){
                return false;
            }
        }
        return true;
    }

    public void addResult(String experimentName, String experimentInput, String experimentResult){
        try {
            String resultExcludingInput = experimentResult.replaceAll(Pattern.quote(experimentInput), "");
            if (newFileIsUnique(resultExcludingInput)) {
                existingResults.add(resultExcludingInput);
                write(experimentName + uid + ".html", HTML_PRE + experimentInput + CONTENT_HEADER + experimentResult + HTML_POST);
                uid++;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
