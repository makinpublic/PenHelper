package tut.makinen.WebSnooper;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class Crawler {
    Set<String> urlsToCrawl;
    Set<String> uniqueUrls;
    ConnectionHandler connectionHandler;

    public Crawler(String rootUrl, ConnectionHandler connectionHandler){
        this.connectionHandler = connectionHandler;
        urlsToCrawl = new HashSet<>();
        urlsToCrawl.add(rootUrl);
        uniqueUrls = new HashSet<>();
    }

    private Set<String> crawl(String url){
        Vector<String> newUrls = HttpParser.parseUrls(connectionHandler.httpGET(url));
        Set<String> returnUrls = new HashSet<>();
        for(String newUrl : newUrls){
            if(!uniqueUrls.contains(newUrl)){
                if(newUrl.contains("http") || "#".equals(newUrl) || "".equals(newUrl)) continue;
                uniqueUrls.add(newUrl);
                returnUrls.add(newUrl);
            }
        }
        return returnUrls;
    }

    public Set<String> crawl(){
        while(true) {
            Iterator<String> iter = urlsToCrawl.iterator();
            Set<String> newUrls = new HashSet<>();
            while (iter.hasNext()) {
                newUrls.addAll(crawl(iter.next()));
                iter.remove();
            }
            if(!newUrls.isEmpty()){
                urlsToCrawl.addAll(newUrls);
            } else {
                break;
            }
        }
        return uniqueUrls;
    }
}
