package tut.makinen.WebSnooper;


import tut.makinen.TinyHTML.Form;

import java.util.LinkedList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class HttpParser {
    private HttpParser(){}

    public static Vector<Form> parseInputs(String http){
        Pattern pattern = Pattern.compile("\\<.+?\\>");
        Matcher matcher = pattern.matcher(http);

        Vector<Form> foundFields = new Vector<>();
        LinkedList<Integer> formStarts = new LinkedList<>();
        LinkedList<Integer> formends = new LinkedList<>();
        while(matcher.find()){
            String found = matcher.group(0);
            if (found.toLowerCase().startsWith("<form")){
                formStarts.add(matcher.start(0));
            } else if (found.toLowerCase().startsWith("</form")){
                formends.add(matcher.end(0));
            }
        }
        for(int i = 0; i < formStarts.size(); i++){
            try {
                foundFields.add(new Form(http.substring(formStarts.get(i), formends.get(i))));
            } catch (Exception e){
                System.out.println("WARNING !!! Mismatching <form> and </form> tags found!");
                e.printStackTrace();
            }
        }

        return foundFields;
    }

    public static Vector<String> parseUrls(String http){
        Pattern pattern = Pattern.compile("\\<.+?>");
        Matcher matcher = pattern.matcher(http);
        Vector<String> foundUrls = new Vector<>();
        while(matcher.find()){
            String found = matcher.group(0);
            if (found.toLowerCase().startsWith("<a ")){
                for(String s : found.toLowerCase().split(" ")){
                    if(s.startsWith("href")){
                        foundUrls.add(s.split("=", 2)[1].replace("\"", "").replace("'", "").replace(">", ""));
                    }
                }
            }
        }
        return foundUrls;
    }
}
