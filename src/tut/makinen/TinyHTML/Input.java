package tut.makinen.TinyHTML;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Input {
    String raw;
    String name;
    String type;
    String value;
    boolean hidden;

    public Input(String raw){
        this.raw = raw;
        hidden = raw.toLowerCase().contains("hidden");
        Pattern namePattern = Pattern.compile("name.*?=.*?\\\'.*?\\\'");
        Pattern typePattern = Pattern.compile("type.*?=.*?\\\'.*?\\\'");
        Pattern valuePattern = Pattern.compile("value.*?=.*?\\\'.*?\\\'");
        Matcher matcher = namePattern.matcher(raw);

        if(matcher.find()){
            name = matcher.group(0);
            name = name.substring(name.indexOf('\'') + 1, name.lastIndexOf('\''));
        } else {
            name = "";
        }

        matcher = typePattern.matcher(raw);
        if(matcher.find()){
            type = matcher.group(0);
            type = type.substring(type.indexOf('\'') + 1, type.lastIndexOf('\''));
        } else {
            type = "";
        }

        matcher = valuePattern.matcher(raw);
        if(matcher.find()){
            value = matcher.group(0);
            value = value.substring(value.indexOf('\'') + 1, value.lastIndexOf('\''));
        } else {
            value = "";
        }
    }

    public boolean hasName(){
        return (this.name.length() > 0);
    }

    public String getName(){
        return this.name;
    }

    public void setValueIfNull(String newValue){
        if(this.value == "" || this.value == null){
            this.value = newValue;
        }
    }

    public void setValue(String value){
        this.value = value;
    }

    public String toUrl(){
        try {
            return name + "=" + value; // URLEncoder.encode(value, "UTF-8");
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String toUrlEncoded(){
        try {
            return name + "=" + URLEncoder.encode(value, "UTF-8");
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String toString(){
        return "<input name=\""+name + "\" type=\"" + type + "\" value=\"" + value + "\" " + (hidden ? "hidden" : "") + ">";
    }

    public String toEscapedString(){
        return "<input name=\\\""+name + "\\\" type=\\\"" + type + "\\\" value=\\\"" + value + "\\\" " + (hidden ? "hidden" : "") + ">";
    }
}
