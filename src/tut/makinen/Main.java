package tut.makinen;

import tut.makinen.WebSnooper.ConnectionHandler;
import tut.makinen.TinyHTML.Form;
import tut.makinen.WebSnooper.Crawler;
import tut.makinen.WebSnooper.HttpParser;
import tut.makinen.WebSnooper.SerialRequester;
import tut.makinen.attacks.SqlInjectionProbe;
import tut.makinen.attacks.XSRF;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class Main {

    private static void printHelp(){
        System.out.println("Commands:" + '\n'
                + "help - this help dialogue" + '\n'
                + "set host|subUrl|cookie|port|folder [value] - set the given parameter" + '\n'
                + "status - show current parameters" + '\n'
                + "crawl - Iterate through urls on page and add them to the list of targets" + '\n'
                + "clearCache - reset list of targets to primary host+subUrl" + '\n'
                + "sqlProbe - show current parameters" + '\n'
                + "xsrf - Create xsrf templates for all targets" + '\n'
                + "xsrfServe - Serve created templates on port 9009" + '\n'
                + "whiteNoise - Run white noise fuzzer against all targets" + '\n'
                + "status - show current parameters" + '\n'
                + "quit - exit the application" + '\n'
        );
    }

    public static void main(String[] args) {
	    System.out.println("Hello!");
        String subUrl = "/661966795123666257345921124794722793837/editprofile.gtl";
        String cookie = "GRUYERE=91830411|eh||author";
	    String host = "localhost";
	    String folder = "C:/results";
        Vector<String> templates = new Vector<>();
        Set<String> targets = new HashSet<>();

	    Integer port = 8008;
	    boolean stay = true;
        BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
	    while(stay) {
            System.out.println("Give command");
            String command = "";
            try {
                command = sysIn.readLine();
            } catch (Exception e) {
                System.out.println("Problem with input, please try again.");
                continue;
            }
            if ("".equals(command)) continue;
            if ("help".equals(command.toLowerCase())) {
                printHelp();
            } else if (command.toLowerCase().startsWith("set")) {
                String[] parameters = command.split(" ");
                if (parameters.length < 3) {
                    System.out.println("Not enough parameters, type 'help' for help");
                } else if (parameters[1].equalsIgnoreCase("host")) {
                    host = parameters[2];
                } else if (parameters[1].equalsIgnoreCase("subUrl")) {
                    subUrl = parameters[2];
                } else if (parameters[1].equals("cookie")) {
                    cookie = parameters[2];
                } else if (parameters[1].equals("port")) {
                    port = Integer.parseInt(parameters[2]);
                } else if (parameters[1].equals("folder")) {
                    folder = parameters[2];
                }
            } else if (command.toLowerCase().equalsIgnoreCase("crawl")) {
                ConnectionHandler ch = new ConnectionHandler(host, port, cookie, subUrl);
                Crawler crwlr = new Crawler(subUrl, ch);
                targets = crwlr.crawl();
                for (String s : crwlr.crawl()) {
                    System.out.println(s);
                }
            } else if (command.toLowerCase().equalsIgnoreCase("clearCache")) {
                targets = new HashSet<>();
            } else if (command.toLowerCase().equalsIgnoreCase("sqlProbe")) {
                //Running for primary
                ConnectionHandler ch = new ConnectionHandler(host, port, cookie, subUrl);
                SqlInjectionProbe sip = new SqlInjectionProbe(ch, subUrl, cookie, folder);
                sip.run();
                //Running for all the other targets
                for (String subTarget : targets) {
                    sip.setTarget(subTarget, cookie);
                    sip.run();
                }
            } else if (command.toLowerCase().equalsIgnoreCase("whiteNoise")) {
                //Running for primary
                ConnectionHandler ch = new ConnectionHandler(host, port, cookie, subUrl);
                String http = ch.httpGET("", subUrl, cookie);
                Vector<Form> forms = HttpParser.parseInputs(http);
                SerialRequester sr = new SerialRequester(forms, ch, subUrl, folder);
                sr.makeRequest(10);
                //Running for all the other targets
                for (String subTarget : targets) {
                    //TODO refactor SerialRequester and possibly ConnectionHandler.
                    // subTarget gets passed around needlessly and a new SerialRequester is made every time
                    http = ch.httpGET("", subTarget, cookie);
                    forms = HttpParser.parseInputs(http);
                    sr = new SerialRequester(forms, ch, subTarget, folder);
                    sr.makeRequest(10);
                }
            } else if (command.toLowerCase().equalsIgnoreCase("xsrf")) {
                ConnectionHandler ch = new ConnectionHandler(host, port, cookie, subUrl);
                XSRF xsrf = new XSRF(ch, folder);
                templates = xsrf.createTemplates();
                for(String subTarget : targets){
                    //TODO Again, needlessly requiring to create new objects just to change the subTarget
                    ch = new ConnectionHandler(host, port, cookie, subTarget);
                    xsrf = new XSRF(ch, folder);
                    templates.addAll(xsrf.createTemplates());
                }
            }else if (command.toLowerCase().equalsIgnoreCase("xsrfServe")){
                ConnectionHandler ch = new ConnectionHandler(host, port, cookie, subUrl);
                if (!templates.isEmpty()) {
                    System.out.println("Serving " + templates.size() + " XSRF templates on port 9009 consecutively");
                    for (String tempalate : templates) {
                        ch.serveHttp(9009, tempalate);
                    }
                } else {
                    System.out.println("No templates to serve. Did you run 'xsrf' yet?");
                }
            }else if (command.toLowerCase().equalsIgnoreCase("status")){
                System.out.println("Status:" + '\n'
                        + "[host]" + host + '\n'
                        + "[port]" + port + '\n'
                        + "[subUrl]" + subUrl + '\n'
                        + "[cookie]" + cookie + '\n'
                        + "[folder]" + folder + '\n'
                        + "[targets] size: " + targets.size() + '\n'
                );
            } else if (command.toLowerCase().equalsIgnoreCase("quit")){
	            stay = false;
	            continue;
            } else {
	            System.out.println("Command not recognized. Please type 'help' for list of commands");
            }
        }
    }
}
