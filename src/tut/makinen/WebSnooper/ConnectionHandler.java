package tut.makinen.WebSnooper;

import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class ConnectionHandler {
    private Charset characterset = Charset.defaultCharset();
    private String host = "localhost";
    private Integer port = 8008; //Gruyere, normal is 80
    private String cookie = null;
    private String subUrl = "";
    private static final String CRLF = "\r\n";

    //TODO POST, PUT, DELETE
    public ConnectionHandler(String host, Integer port){
        if(host != null) this.host = host;
        if(port != null) this.port = port;
    }

    public ConnectionHandler(String host, Integer port, String cookie, String subUrl){
        this(host, port);
        if(cookie != null) this.cookie = cookie;
        if(subUrl != null) this.subUrl = subUrl;
    }

    public String stripHttpHeader(String http){
        //System.out.println(http);
        if(!http.contains(CRLF+CRLF)) return http;
        return http.split(CRLF+CRLF, 2)[1];
    }

    public String toString(){
        return "http://" + host + ":" + port + subUrl;
    }

    public String httpGET(){
        return httpGET("", subUrl, cookie);
    }

    public String httpGET(String subUrl){
        return httpGET("", subUrl, cookie);
    }

    public String httpGET(String messageBody, String subUrl, String cookie){
        System.out.println("Processing url: http://" + host + ":" + port + subUrl);
        String receivedMessage = "";
        try {
            Socket socket = new Socket(this.host, this.port);
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            //TODO
            pw.print("GET "+subUrl+" HTTP/1.1" + CRLF);
            if(cookie != null){
                pw.print("Cookie: " + cookie + CRLF);
            }
            pw.print(CRLF);
            pw.print(messageBody);
            pw.print(CRLF);
            pw.flush();
            InputStream inputStream = socket.getInputStream();

            int character;
            int consecutiveNewLines = 0;
            while((character = inputStream.read()) != -1){
                receivedMessage += (char)character;
                //System.out.print((char)character + (character == 13 ||  character == 10 ? "(" + character + ")" : ""));

                //Some webpages, like Google, end the message with 2 newlines. But 2 newlines also indicate end of header.
                if(receivedMessage.endsWith(CRLF+CRLF)){
                    consecutiveNewLines++;
                    if(consecutiveNewLines == 2) break;
                }
                //if(receivedMessage.endsWith("\r\r\r\n")) break;
            }
            //System.out.println("Reached end. Closing connection.");
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return receivedMessage;
    }

    public void serveHttp(int port, String body){
        try {
            System.out.println("Serving single serving of HTTP on port "+ port);
            ServerSocket serverSocket = new ServerSocket(port);
            Socket client = serverSocket.accept();
            PrintWriter out = new PrintWriter(client.getOutputStream());
            out.println("HTTP/1.1 200 OK");
            out.println("Content-Type: text/html");
            out.println(CRLF);
            out.println(body);
            out.flush();
            out.close();
            client.close();
            serverSocket.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
