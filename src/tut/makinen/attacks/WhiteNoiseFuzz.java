package tut.makinen.attacks;

import tut.makinen.TinyHTML.Form;
import tut.makinen.TinyHTML.Input;
import tut.makinen.util.Rand;

import java.util.Vector;

public class WhiteNoiseFuzz implements Attack{

    static final boolean decoded = true;

    public static String formToNoiseUrl(Form form){
        Vector<Input> selectedInputs = new Vector<>();
        Rand rand = new Rand();
        for(Input input : form.getInputs()){
            if(rand.nextBoolean()){
                selectedInputs.add(input);
            }
        }

        if(form.getSubmit().size() == 0) {

        } else if (form.getSubmit().size() == 1){
            selectedInputs.add(form.getSubmit().get(0));
        } else {
            selectedInputs.add(form.getSubmit().get(rand.nextInt(form.getSubmit().size() - 1)));
        }

        String url = inputToNoiseUrl(selectedInputs, null, 3);
        return url;
    }

    public static String inputToNoiseUrl(Vector<Input> selectedInputs, Input selectedSubmit, int stringLevel){
        Rand rand = new Rand();
        String url = "";
        for(Input input : selectedInputs){
            input.setValue(rand.nextString(rand.nextInt(50), stringLevel));
            url += (url.length() > 0 ? "&" : "") + (decoded ? input.toUrlEncoded() : input.toUrl());
        }

        if(selectedSubmit != null && selectedSubmit.hasName()){
            url += (decoded ? selectedSubmit.toUrlEncoded() : selectedSubmit.toUrl());
        }

        return url;
    }
}
