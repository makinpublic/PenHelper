package tut.makinen.TinyHTML;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Form {
    String raw;
    Vector<Input> input;
    Vector<Input> submit;

    public Form(String raw){
        this.raw = raw;
        Pattern pattern = Pattern.compile("\\<.+?\\>");
        Matcher matcher = pattern.matcher(raw);

        input = new Vector<>();
        submit = new Vector<>();
        while(matcher.find()){
            String found = matcher.group(0);
            if(found.toLowerCase().startsWith("<input")){
                Input input = new Input(found);
                if("submit".equals(input.type.toLowerCase())){
                    this.submit.add(input);
                } else {
                    this.input.add(input);
                }
            }
        }
    }

    public Vector<Input> getInputs(){
        return input;
    }

    public void fillEmptyValues(String fill){
        for(Input inp : input){
            inp.setValueIfNull(fill);
        }
    }

    //That is, 3 inputs would result in 000, 001, 010, 100, 101, 110, 011, 111
    public Vector<Vector<Input>> getAllInputCombinations(){
        Vector<Vector<Input>> returnVector = new Vector<>();
        double max = Math.pow(2, input.size()) - 1;
        for(int i = 0; i <= max; i++){
            String sBinary = Integer.toBinaryString(i);
            sBinary = String.format("%"+Integer.toBinaryString((int) max).length()+"s", sBinary).replace(" ", "0");
            Vector<Input> innerVector = new Vector<>();

            for(int ii = 0; ii < sBinary.length(); ii++){
                if(sBinary.charAt(ii) == '1'){
                    innerVector.add(input.get(ii));
                }
            }

            returnVector.add(innerVector);
        }
        return returnVector;
    }

    public Vector<Input> getSubmit(){
        return submit;
    }

    public String toUrl(Input submit){
        String url = "";
        for(Input inp : input){
            url += (url.length() > 0 ? "&" : "") + inp.toUrl();
        }
        if(submit != null){
            url = submit.toUrl() + (url.length() > 0 ? "&" : "");
        }
        return url;
    }
}
